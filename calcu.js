const fs = require('fs');

const calculadora = {}
function generateLog (x, y, op, result) {
   const mensaje = " \n" + x + op + y + "=" + result;
    fs.writeFileSync('log.txt', mensaje ,{flag:"a"} );   
}
function suma(x,y) {
    generateLog(x, y, "+", x+y )
}

suma(4,7)

function resta(x,y) {
    generateLog(x, y, "-", x-y )
}
resta(5,2)

function dividir(x,y) {
    generateLog(x, y, "/", x/y )
}

dividir(10,2)